<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    protected $table = 'trabajadores';
    protected $primaryKey = 'ci';
    protected  $keyType = 'string';
    protected $fillable = ['ci','nombre', 'nombre2', 'apellido', 'apellido2',
        'email', 'email2', 'telefono', 'telefono2', 'ingreso', 'egreso', 'nacimiento'];
    public $incrementing = false;

}
