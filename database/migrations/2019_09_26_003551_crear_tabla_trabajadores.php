<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaTrabajadores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trabajadores', function (Blueprint $table) {
            $table->string('ci')->primary();
            $table->string('nombre', 30);
            $table->string('nombre2', 30)->nullable();
            $table->string('apellido', 30);
            $table->string('apellido2', 30)->nullable();
            $table->string('email', 100)->unique();
            $table->string('email2', 100)->nullable();
            $table->string('direccion', 500)->nullable();
            $table->string('telefono', 20)->nullable();
            $table->string('telefono2', 20)->nullable();
            $table->timestamp('ingreso');
            $table->timestamp('egreso')->nullable();
            $table->timestamp('nacimiento')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trabajadores');
    }
}
